package main

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/pbkdf2"
)

type Client interface {
	Auth(user string, pass string) (*Bot, error)
	Vote(bot Bot, article Article) (*Vote, error)
	ListHots(bot Bot, page int) ([]Article, error)
	ListFollows(bot Bot, page int) ([]Article, error)
	ListByAuthor(bot Bot, page int, user int) ([]Article, error)
}

const baseUrl = "https://be02.bihu.com/bihube-pc/api"

var DefaultHdr = http.Header{}

type GoHttpClient struct {
	*http.Client
	hdr http.Header
}

func NewClient(hdr http.Header) *GoHttpClient {
	return &GoHttpClient{
		Client: &http.Client{},
		hdr:    hdr,
	}
}

func (c *GoHttpClient) Auth(user string, pass string) (*Bot, error) {
	form := url.Values{}
	form.Set("phone", user)
	form.Set("password", mkPass(pass))

	logger := log.WithField(
		"api", "/user/loginViaPassword",
	)
	if log.GetLevel() >= log.DebugLevel {
		logger = logger.WithField("args", form.Encode())
	}

	req, err := c.consReq(
		"/user/loginViaPassword",
		"https://bihu.com/login",
		form, logger,
	)
	if err != nil {
		return nil, err
	}

	res, err := c.doReq(req, logger)
	if err != nil {
		return nil, err
	}

	var ret struct {
		Res    int    `json:"res"`
		ResMsg string `json:"resMsg"`
		Data   struct {
			AccessToken string `json:"accessToken"`
			UserId      string `json:"userId"`
		} `json:"data"`
	}
	err = json.Unmarshal(res, &ret)
	if ret.Res == 1 && ret.Data.AccessToken != "" && ret.Data.UserId != "" {
		uid, err := strconv.Atoi(ret.Data.UserId)
		if err != nil {
			log.Errorf("user id should be numeric, got %s", ret.Data.UserId)
			return nil, err
		}
		return &Bot{
			Id:     int64(uid),
			Login:  user,
			Token:  ret.Data.AccessToken,
			AuthAt: time.Now().Unix(),
		}, nil
	}

	err = wrapErr(err, res)
	logger.WithField("res", string(res)).WithError(err).Warn("cannot get access token")
	return nil, err
}

func (c *GoHttpClient) Vote(bot Bot, article Article) (*Vote, error) {
	form := bot.Form()
	form.Set("artId", strconv.Itoa(int(article.Id)))

	logger := log.WithField(
		"api", "/content/upVote",
	).WithField("bot", bot).WithField("article", article)
	if log.GetLevel() >= log.DebugLevel {
		logger = logger.WithField("args", form.Encode())
	}

	req, err := c.consReq(
		"/content/upVote",
		fmt.Sprintf("https://bihu.com/article/%d", article.Id),
		form, logger,
	)
	if err != nil {
		return nil, err
	}

	// do request
	res, err := c.doReq(req, logger)
	if err != nil {
		return nil, err
	}

	var ret struct {
		Res    int    `json:"res"`
		ResMsg string `json:"resMsg"`
	}
	err = json.Unmarshal(res, &ret)
	if ret.Res != 1 && !strings.HasPrefix(ret.ResMsg, "已投过票了") {
		err = wrapErr(err, res)
		logger.WithField("res", string(res)).WithError(err).Warn("vote failed")
		return nil, err
	}

	return &Vote{
		BotId:     bot.Id,
		ArticleId: article.Id,
		VotedAt:   time.Now().Unix(),
	}, nil
}

func (c *GoHttpClient) ListHots(bot Bot, page int) ([]Article, error) {
	form := bot.Form()
	if page > 1 {
		form.Set("pageNum", strconv.Itoa(page))
	}

	logger := log.WithField(
		"api", "/content/show/hotArtList",
	)
	if log.GetLevel() >= log.DebugLevel {
		logger = logger.WithField("args", form.Encode())
	}

	req, err := c.consReq(
		"/content/show/hotArtList",
		"https://bihu.com/",
		form, logger,
	)
	if err != nil {
		return nil, err
	}

	res, err := c.doReq(req, logger)
	if err != nil {
		return nil, err
	}

	var ret struct {
		Res    int    `json:"res"`
		ResMsg string `json:"resMsg"`
		Data   struct {
			List []struct {
				Id         int64   `json:"id"`
				Title      string  `json:"title"`
				CreateTime int64   `json:"createTime"`
				Ups        int     `json:"ups"`
				Cmts       int     `json:"cmts"`
				Money      float64 `json:"money"`
			} `json:"list"`
		} `json:"data"`
	}
	err = json.Unmarshal(res, &ret)
	if ret.Res != 1 {
		err = wrapErr(err, res)
		logger.WithField("res", string(res)).WithError(err).Warn("list hots failed")
		return nil, err
	}
	arts, now := make([]Article, len(ret.Data.List)), time.Now().Unix()
	for i, o := range ret.Data.List {
		arts[i].Id = o.Id
		arts[i].Title = o.Title
		arts[i].CreatedAt = o.CreateTime / 1000
		arts[i].SyncAt = now
		arts[i].Ups = o.Ups
		arts[i].Comments = o.Cmts
		arts[i].Money = o.Money
	}
	return arts, nil
}

func (c *GoHttpClient) ListFollows(bot Bot, page int) ([]Article, error) {
	form := bot.Form()
	if page > 1 {
		form.Set("pageNum", strconv.Itoa(page))
	}

	logger := log.WithField(
		"api", "/content/show/getFollowArtList",
	)
	if log.GetLevel() >= log.DebugLevel {
		logger = logger.WithField("args", form.Encode())
	}

	req, err := c.consReq(
		"/content/show/getFollowArtList",
		"https://bihu.com/?category=follow",
		form, logger,
	)
	if err != nil {
		return nil, err
	}

	res, err := c.doReq(req, logger)
	if err != nil {
		return nil, err
	}

	var ret struct {
		Res    int    `json:"res"`
		ResMsg string `json:"resMsg"`
		Data   struct {
			ArtList struct {
				List []struct {
					Id         int64   `json:"id"`
					Title      string  `json:"title"`
					CreateTime int64   `json:"createTime"`
					Ups        int     `json:"ups"`
					Cmts       int     `json:"cmts"`
					Money      float64 `json:"money"`
				} `json:"list"`
			} `json:"artList"`
		} `json:"data"`
	}
	err = json.Unmarshal(res, &ret)
	if ret.Res != 1 {
		err = wrapErr(err, res)
		logger.WithField("res", string(res)).WithError(err).Warn("list follows failed")
		ioutil.WriteFile(fmt.Sprintf("%d.oops", time.Now().Unix()), res, 0644)
		return nil, err
	}
	arts, now := make([]Article, len(ret.Data.ArtList.List)), time.Now().Unix()
	for i, o := range ret.Data.ArtList.List {
		arts[i].Id = o.Id
		arts[i].Title = o.Title
		arts[i].CreatedAt = o.CreateTime / 1000
		arts[i].SyncAt = now
		arts[i].Ups = o.Ups
		arts[i].Comments = o.Cmts
		arts[i].Money = o.Money
	}
	return arts, nil
}

func (c *GoHttpClient) ListByAuthor(bot Bot, page int, user int) ([]Article, error) {
	form := bot.Form()
	form.Set("queryUserId", strconv.Itoa(user))
	if page > 1 {
		form.Set("pageNum", strconv.Itoa(page))
	}

	logger := log.WithField(
		"api", "/content/show/getUserArtList",
	)
	if log.GetLevel() >= log.DebugLevel {
		logger = logger.WithField("args", form.Encode())
	}

	req, err := c.consReq(
		"/content/show/getUserArtList",
		fmt.Sprintf("https://bihu.com/people/%d", user),
		form, logger,
	)
	if err != nil {
		return nil, err
	}

	res, err := c.doReq(req, logger)
	if err != nil {
		return nil, err
	}

	var ret struct {
		Res    int    `json:"res"`
		ResMsg string `json:"resMsg"`
		Data   struct {
			List []struct {
				Id         int64   `json:"id"`
				Title      string  `json:"title"`
				CreateTime int64   `json:"createTime"`
				Ups        int     `json:"ups"`
				Cmts       int     `json:"cmts"`
				Money      float64 `json:"money"`
			} `json:"list"`
		} `json:"data"`
	}
	err = json.Unmarshal(res, &ret)
	if ret.Res != 1 {
		err = wrapErr(err, res)
		logger.WithField("res", string(res)).WithError(err).Warn("list by user failed")
		return nil, err
	}
	arts, now := make([]Article, len(ret.Data.List)), time.Now().Unix()
	for i, o := range ret.Data.List {
		arts[i].Id = o.Id
		arts[i].Title = o.Title
		arts[i].CreatedAt = o.CreateTime / 1000
		arts[i].SyncAt = now
		arts[i].Ups = o.Ups
		arts[i].Comments = o.Cmts
		arts[i].Money = o.Money
	}
	return arts, nil
}

func (c *GoHttpClient) consReq(api string, referer string, form url.Values, logger *log.Entry) (*http.Request, error) {
	req, err := http.NewRequest(
		http.MethodPost, baseUrl+api,
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		err = errors.Wrap(err, "construct request")
		logger.WithError(err).Debug()
		return nil, err
	}
	copyHdr(req.Header, c.hdr)
	req.Header.Set("Referer", referer)

	return req, err
}

func (c *GoHttpClient) doReq(req *http.Request, logger *log.Entry) ([]byte, error) {
	resp, err := c.Do(req)
	if err != nil {
		err = errors.Wrap(err, "perform request")
		logger.WithError(err).Debug()
		return nil, err
	}
	defer resp.Body.Close()

	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = errors.Wrap(err, "read response")
		logger.WithError(err).Debug()
		return nil, err
	}
	return res, nil
}

func mkPass(pass string) string {
	bsPass := []byte(pass)
	salt := md5.Sum(bsPass)
	bsSalt := []byte(hex.EncodeToString(salt[:]))
	return hex.EncodeToString(pbkdf2.Key(bsPass, bsSalt, 2e3, 32, sha256.New))
}

func copyHdr(to http.Header, from http.Header) {
	for k, v := range from {
		to[k] = v
	}
}

func wrapErr(err error, res []byte) error {
	if res[0] == 0x1f {
		return errors.New("you might be blocked")
	} else if err != nil {
		return errors.Wrap(err, "parse response")
	} else {
		return errors.New(string(res))
	}
}

func init() {
	DefaultHdr.Add("Accept", "*/*")
	DefaultHdr.Add("Accept-Encoding", "gzip, deflate, br")
	DefaultHdr.Add("Accept-Language", "en,zh;q=0.9")
	DefaultHdr.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
	DefaultHdr.Add("Origin", "https://bihu.com")
	DefaultHdr.Add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8")
}
