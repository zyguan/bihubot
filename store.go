package main

import (
	"bytes"
	"fmt"
	"time"

	"bitbucket.org/zyguan/bihubot/tab"
	"github.com/gocraft/dbr"
	"github.com/gocraft/dbr/dialect"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type Store interface {
	Init() error
	GetBot(login string) (*Bot, error)
	PutBot(bot Bot) (*Bot, error)
	PutArticles(article []Article) ([]Article, error)
	RecordVote(bot int64, article int64) (*Vote, error)
	ResolveArticleForVote(bot int64, maxUps int, expiry time.Duration, money float64) ([]Article, error)
}

type SqliteStore struct {
	DB *dbr.Connection
}

func (x *SqliteStore) Init() error {
	s := x.DB.NewSession(nil)
	for _, ddl := range []string{
		tab.BotDDL, tab.ArticleDDL, tab.VoteDDL,
	} {
		if _, err := s.Exec(ddl); err != nil {
			err = errors.Wrap(err, "create table")
			log.WithField("ddl", ddl).WithError(err).Error()
			return err
		}
	}
	return nil
}

func (x *SqliteStore) GetBot(login string) (*Bot, error) {
	s := x.DB.NewSession(nil)

	var bot Bot
	err := s.Select(
		tab.Bot.Cols.Id,
		tab.Bot.Cols.Login,
		tab.Bot.Cols.Token,
		tab.Bot.Cols.AuthAt,
	).From(
		tab.Bot.Name,
	).Where(
		dbr.Eq(tab.Bot.Cols.Login, login),
	).LoadOne(&bot)

	if err != nil {
		return nil, errors.Wrap(err, "get bot")
	}

	return &bot, nil
}

func (x *SqliteStore) PutBot(bot Bot) (*Bot, error) {
	s := x.DB.NewSession(nil)

	query := fmt.Sprintf(`
		INSERT OR REPLACE INTO %s (%s,%s,%s,%s) VALUES (?,?,?,?)`,
		tab.Bot.Name,
		tab.Bot.Cols.Id,
		tab.Bot.Cols.Login,
		tab.Bot.Cols.Token,
		tab.Bot.Cols.AuthAt,
	)

	_, err := s.Exec(query, bot.Id, bot.Login, bot.Token, bot.AuthAt)

	if err != nil {
		return nil, errors.Wrap(err, "put bot")
	}

	return &bot, nil
}

func (x *SqliteStore) PutArticles(arts []Article) ([]Article, error) {
	s := x.DB.NewSession(nil)

	partial := fmt.Sprintf(`
		INSERT OR REPLACE INTO %s (%s,%s,%s,%s,%s,%s,%s,%s) VALUES`,
		tab.Article.Name,
		tab.Article.Cols.Id,
		tab.Article.Cols.Title,
		tab.Article.Cols.AuthorId,
		tab.Article.Cols.CreatedAt,
		tab.Article.Cols.SyncAt,
		tab.Article.Cols.Ups,
		tab.Article.Cols.Comments,
		tab.Article.Cols.Money,
	)

	var buf bytes.Buffer
	args := make([]interface{}, 0, 8*len(arts))
	buf.Write([]byte(partial))
	for i, art := range arts {
		sep := ","
		if i == 0 {
			sep = " "
		}
		buf.Write([]byte(sep))
		buf.Write([]byte("(?,?,?,?,?,?,?,?)"))
		args = append(args, art.Id)
		args = append(args, art.Title)
		args = append(args, art.AuthorId)
		args = append(args, art.CreatedAt)
		args = append(args, art.SyncAt)
		args = append(args, art.Ups)
		args = append(args, art.Comments)
		args = append(args, art.Money)
	}

	_, err := s.Exec(buf.String(), args...)

	if err != nil {
		return nil, errors.Wrap(err, "put article")
	}

	return arts, nil
}

func (x *SqliteStore) RecordVote(bot int64, article int64) (*Vote, error) {
	s := x.DB.NewSession(nil)
	now := time.Now().Unix()
	_, err := s.InsertInto(
		tab.Vote.Name,
	).Columns(
		tab.Vote.Cols.BotId,
		tab.Vote.Cols.ArticleId,
		tab.Vote.Cols.VotedAt,
	).Values(
		bot, article, now,
	).Exec()
	if err != nil {
		return nil, errors.Wrap(err, "record vote")
	}
	return &Vote{bot, article, now}, nil
}

func (x *SqliteStore) ResolveArticleForVote(bot int64, maxUps int, expiry time.Duration, money float64) ([]Article, error) {
	s := x.DB.NewSession(nil)

	buf := dbr.NewBuffer()
	err := dbr.Select(
		tab.Vote.Cols.ArticleId,
	).From(
		tab.Vote.Name,
	).Where(
		dbr.Eq(tab.Vote.Cols.BotId, bot),
	).Build(dialect.SQLite3, buf)

	if err != nil {
		return nil, err
	}

	b := s.Select(
		tab.Article.Cols.Id,
		tab.Article.Cols.Title,
		tab.Article.Cols.AuthorId,
		tab.Article.Cols.CreatedAt,
		tab.Article.Cols.SyncAt,
		tab.Article.Cols.Ups,
		tab.Article.Cols.Comments,
		tab.Article.Cols.Money,
	).From(
		tab.Article.Name,
	).Where(
		dbr.Expr(fmt.Sprintf(
			"%s NOT IN (%s)",
			tab.Article.Cols.Id,
			buf.String(),
		), buf.Value()...),
	)
	if maxUps > 0 {
		b = b.Where(
			dbr.Lt(tab.Article.Cols.Ups, maxUps),
		)
	}
	if expiry > 0 {
		b = b.Where(
			dbr.Gt(tab.Article.Cols.CreatedAt, time.Now().Unix()-int64(expiry/time.Second)),
		)
	}
	if money > 0 {
		b = b.Where(
			dbr.Gt(tab.Article.Cols.Money, money),
		)
	}

	b = b.OrderDir(tab.Article.Cols.Money, false)

	var as []Article
	if _, err = b.Load(&as); err != nil {
		return nil, err
	}

	return as, nil
}
