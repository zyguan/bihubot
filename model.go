package main

import (
	"fmt"
	"net/url"
	"strconv"
	"time"
)

type Bot struct {
	Id     int64
	Login  string
	Token  string
	AuthAt int64
}

func (b *Bot) String() string {
	return fmt.Sprintf("B#%d:%s", b.Id, b.Token)
}

func (b *Bot) Form() url.Values {
	form := url.Values{}
	if b.Id > 0 {
		form.Set("userId", strconv.Itoa(int(b.Id)))
		form.Set("accessToken", b.Token)
	}
	return form
}

type Article struct {
	Id        int64
	Title     string
	AuthorId  int64
	CreatedAt int64
	SyncAt    int64
	Ups       int
	Comments  int
	Money     float64
}

func (a *Article) String() string {
	return fmt.Sprintf("A#%d:%d[%s]", a.Id, a.Ups, time.Unix(a.CreatedAt, 0))
}

type Vote struct {
	BotId     int64
	ArticleId int64
	VotedAt   int64
}

func (v *Vote) String() string {
	return fmt.Sprintf("V#%d:%d", v.BotId, v.ArticleId)
}
