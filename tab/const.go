package tab

const BotDDL = `
CREATE TABLE IF NOT EXISTS bot (
  id      INTEGER PRIMARY KEY,
  login   VARCHAR(255) NOT NULL,
  token   VARCHAR(255) NOT NULL,
  auth_at INTEGER      NOT NULL,
  UNIQUE (login)
);
`

type BotCols struct {
	Id     string // type: int(11), opts: [pk]
	Login  string // type: varchar(255), opts: [!nil]
	Token  string // type: varchar(255), opts: [!nil]
	AuthAt string // type: int(11), opts: [!nil]
}

var Bot = struct {
	Name string
	Cols BotCols
}{
	Name: "bot",
	Cols: BotCols{
		Id:     "id",
		Login:  "login",
		Token:  "token",
		AuthAt: "auth_at",
	},
}

const ArticleDDL = `
CREATE TABLE IF NOT EXISTS article (
  id         INTEGER PRIMARY KEY,
  title      VARCHAR(255) DEFAULT 'UNTITLED',
  author_id  INTEGER NOT NULL,
  created_at INTEGER NOT NULL,
  sync_at    INTEGER NOT NULL,
  ups        INTEGER NOT NULL DEFAULT 0,
  comments   INTEGER NOT NULL DEFAULT 0,
  money      DECIMAL(10, 2) NOT NULL DEFAULT 0
);
`

type ArticleCols struct {
	Id        string // type: int(11), opts: [pk]
	Title     string // type: varchar(255), opts: [def="UNTITLED"]
	AuthorId  string // type: int(11), opts: [!nil]
	CreatedAt string // type: int(11), opts: [!nil]
	SyncAt    string // type: int(11), opts: [!nil]
	Ups       string // type: int(11), opts: [!nil def=0]
	Comments  string // type: int(11), opts: [!nil def=0]
	Money     string // type: decimal(10,2), opts: [!nil def=0]
}

var Article = struct {
	Name string
	Cols ArticleCols
}{
	Name: "article",
	Cols: ArticleCols{
		Id:        "id",
		Title:     "title",
		AuthorId:  "author_id",
		CreatedAt: "created_at",
		SyncAt:    "sync_at",
		Ups:       "ups",
		Comments:  "comments",
		Money:     "money",
	},
}

const VoteDDL = `
CREATE TABLE IF NOT EXISTS vote (
  bot_id     VARCHAR(255) NOT NULL,
  article_id INTEGER      NOT NULL,
  voted_at   INTEGER      NOT NULL,
  UNIQUE (bot_id, article_id)
);
`

type VoteCols struct {
	BotId     string // type: varchar(255), opts: [!nil]
	ArticleId string // type: int(11), opts: [!nil]
	VotedAt   string // type: int(11), opts: [!nil]
}

var Vote = struct {
	Name string
	Cols VoteCols
}{
	Name: "vote",
	Cols: VoteCols{
		BotId:     "bot_id",
		ArticleId: "article_id",
		VotedAt:   "voted_at",
	},
}
