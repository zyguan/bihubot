CREATE TABLE IF NOT EXISTS bot (
  id      INTEGER PRIMARY KEY,
  login   VARCHAR(255) NOT NULL,
  token   VARCHAR(255) NOT NULL,
  auth_at INTEGER      NOT NULL,
  UNIQUE (login)
);
CREATE TABLE IF NOT EXISTS article (
  id         INTEGER PRIMARY KEY,
  title      VARCHAR(255) DEFAULT 'UNTITLED',
  author_id  INTEGER NOT NULL,
  created_at INTEGER NOT NULL,
  sync_at    INTEGER NOT NULL,
  ups        INTEGER NOT NULL DEFAULT 0,
  comments   INTEGER NOT NULL DEFAULT 0,
  money      DECIMAL(10, 2) NOT NULL DEFAULT 0
);
CREATE TABLE IF NOT EXISTS vote (
  bot_id     VARCHAR(255) NOT NULL,
  article_id INTEGER      NOT NULL,
  voted_at   INTEGER      NOT NULL,
  UNIQUE (bot_id, article_id)
);
