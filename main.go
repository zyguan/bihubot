package main

import (
	"flag"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/gocraft/dbr"
	log "github.com/sirupsen/logrus"
)

var store Store

type _Inspector struct {
	*dbr.NullEventReceiver
}

func (i *_Inspector) TimingKv(eventName string, nanoseconds int64, kvs map[string]string) {
	if strings.HasPrefix(eventName, "dbr.select") || strings.HasPrefix(eventName, "dbr.select") {
		if log.GetLevel() >= log.DebugLevel {
			l := log.WithField("makespan", time.Duration(nanoseconds)*time.Nanosecond)
			for k, v := range kvs {
				l = l.WithField(k, v)
			}
			l.Debug(eventName)
		}
	}
}

func initStore() {
	db, err := dbr.Open("sqlite3", "store.db",
		&_Inspector{&dbr.NullEventReceiver{}})
	if err != nil {
		log.WithError(err).Fatal("unable to open data store")
	}
	store = &SqliteStore{DB: db}
	if err := store.Init(); err != nil {
		log.WithError(err).Fatal("unable to init data store")
	}
}

func updateBot(user string, pass string) {
	cli := NewClient(DefaultHdr)
	bot, err := cli.Auth(user, pass)
	if err != nil {
		log.WithError(err).Fatal("authentication failed")
	}
	_, err = store.PutBot(*bot)
	if err != nil {
		log.WithError(err).Fatal("failed to save bot")
	}
	log.Infof("new bot info: %s", bot)
}

var opts struct {
	period  float64
	stddev  float64
	store   string
	pages   uint64
	debug   bool
	user    string
	pass    string
	ups     uint
	expiry  uint
	money   float64
	workers uint
	follows bool
}

func main() {
	flag.Float64Var(&opts.period, "period", 60.0, "period of fetching articles in second")
	flag.Float64Var(&opts.stddev, "stddev", 10.0, "stddev of fetching period in second")
	flag.StringVar(&opts.store, "store", "store.db", "sqlite3 dsn for data store")
	flag.Uint64Var(&opts.pages, "pages", uint64(1), "no. pages to resolve")
	flag.BoolVar(&opts.debug, "debug", false, "print debug log")
	flag.StringVar(&opts.user, "user", "", "user to run as bot")
	flag.StringVar(&opts.pass, "pass", "", "password of user (non-empty for update bot)")
	flag.UintVar(&opts.ups, "ups", 100, "only vote for article with ups less than this value")
	flag.UintVar(&opts.expiry, "expiry", 0, "only vote for article created in this duration (in second)")
	flag.Float64Var(&opts.money, "money", 0, "only vote for article with money greater than this value")
	flag.UintVar(&opts.workers, "workers", 2, "no. workers to vote")
	flag.BoolVar(&opts.follows, "follows", true, "fetch follows, or else hots")
	flag.Parse()
	initStore()

	if opts.debug {
		log.SetLevel(log.DebugLevel)
	}

	if opts.user == "" {
		log.Fatal("-user cannot be empty")
	}

	if opts.pass != "" {
		updateBot(opts.user, opts.pass)
		return
	}

	bot, err := store.GetBot(opts.user)
	if err != nil {
		log.Fatalf("no such bot, please init the bot by given -pass")
	}
	log.WithField("opts", fmt.Sprintf("%+v", opts)).Infof("start bot %s", bot)

	if opts.pages > 0 {
		opts.pages -= 1
	}

	if opts.workers == 0 {
		opts.workers = 1
	}

	cli, fakeBot := NewClient(DefaultHdr), Bot{}

	rs := resolveVRs(*bot)
	for i := uint(0); i < opts.workers; i++ {
		go runWorker(i, cli, rs)
	}

	fetch := func(page int) ([]Article, error) {
		return cli.ListFollows(*bot, page)
	}
	if !opts.follows {
		fetch = func(page int) ([]Article, error) {
			return cli.ListHots(fakeBot, page)
		}
	}

	zipf := rand.NewZipf(rand.New(rand.NewSource(time.Now().Unix())), 1.2, 1.1, opts.pages)
	for {
		page := int(zipf.Uint64()) + 1
		logger := log.WithField("page", page).WithField("follows", opts.follows)
		logger.Info("start to fetch articles")
		arts, err := fetch(page)
		if err != nil || len(arts) == 0 {
			logger.WithError(err).Warn("failed to fetch articles")
		} else {
			_, err = store.PutArticles(arts)
			if err != nil {
				logger.WithError(err).Fatal("failed to save articles")
			}
		}
		time.Sleep(time.Duration(((rand.NormFloat64() * opts.stddev) + opts.period) * float64(time.Second)))
	}
}

func init() {
	formatter := &log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	}
	log.SetFormatter(formatter)
}

type VoteRequest struct {
	Bot Bot
	Art Article
}

func resolveVRs(bot Bot) <-chan VoteRequest {
	rs := make(chan VoteRequest, opts.workers)
	d := time.Duration(opts.period / 4 * float64(time.Second))
	if d > time.Second {
		d = time.Second
	}
	go func() {
		for {
			time.Sleep(d)
			arts, err := store.ResolveArticleForVote(bot.Id, int(opts.ups),
				time.Duration(opts.expiry)*time.Second, opts.money)
			if err != nil {
				log.WithError(err).Warn("error in resolving vote requests")
			}
			if len(arts) > 0 {
				log.Infof("articles to vote: %v", arts)
			}
			for _, art := range arts {
				rs <- VoteRequest{Bot: bot, Art: art}
			}
		}
	}()
	return rs
}

func runWorker(id uint, cli Client, rs <-chan VoteRequest) {
	logger := log.WithField("worker", strconv.Itoa(int(id)))
	for r := range rs {
		logger.Infof("%s vote for %s", r.Bot.String(), r.Art.String())
		vote, err := cli.Vote(r.Bot, r.Art)
		if err != nil {
			log.WithError(err).Warn("vote failed")
			continue
		}
		_, err = store.RecordVote(vote.BotId, vote.ArticleId)
		if err != nil {
			log.WithError(err).Warn("maybe voted")
		}
	}
}
